package domain;

public class User {
	private String username;
	private String password;
	private String email;
	private boolean canRegister = true;
	private boolean canLogIn = true;
	private boolean canSeeProfile = false;
	private boolean canAccessPremium = false;
	
	private boolean canEditPosts = false;
	private boolean canPublishPosts = false;
	private boolean canChangeOptions = false;
	private boolean canPromoteAdmins = false; 
	private boolean canAccessSubpages = false;
	
	
	public boolean isCanAccessSubpages() {
		return canAccessSubpages;
	}
	public void setCanAccessSubpages(boolean canAccessSubpages) {
		this.canAccessSubpages = canAccessSubpages;
	}
	public boolean isCanRegister() {
		return canRegister;
	}
	public void setCanRegister(boolean canRegister) {
		this.canRegister = canRegister;
	}
	public boolean isCanLogIn() {
		return canLogIn;
	}
	public void setCanLogIn(boolean canLogIn) {
		this.canLogIn = canLogIn;
	}
	public boolean isCanSeeProfile() {
		return canSeeProfile;
	}
	public void setCanSeeProfile(boolean canSeeProfile) {
		this.canSeeProfile = canSeeProfile;
	}
	public boolean isCanAccessPremium() {
		return canAccessPremium;
	}
	public void setCanAccessPremium(boolean canAccessPremium) {
		this.canAccessPremium = canAccessPremium;
	}
	public boolean isCanEditPosts() {
		return canEditPosts;
	}
	public void setCanEditPosts(boolean canEditPosts) {
		this.canEditPosts = canEditPosts;
	}
	public boolean isCanPublishPosts() {
		return canPublishPosts;
	}
	public void setCanPublishPosts(boolean canPublishPosts) {
		this.canPublishPosts = canPublishPosts;
	}
	public boolean isCanChangeOptions() {
		return canChangeOptions;
	}
	public void setCanChangeOptions(boolean canChangeOptions) {
		this.canChangeOptions = canChangeOptions;
	}
	public boolean isCanPromoteAdmins() {
		return canPromoteAdmins;
	}
	public void setCanPromoteAdmins(boolean canPromoteAdmins) {
		this.canPromoteAdmins = canPromoteAdmins;
	}

	
	public String getName() {
		return username;
	}
	public void setName(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


}
