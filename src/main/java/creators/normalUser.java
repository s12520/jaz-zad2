package creators;


import domain.User;

public class normalUser extends User {
	
	public static User createNormal(){ 
	
		User cos = new User();

	
	cos.setName("Tomasz");
	cos.setPassword("password");
	cos.setEmail("email");
	cos.setCanLogIn(false);
	cos.setCanRegister(false);
	cos.setCanSeeProfile(true);
	cos.setCanEditPosts(true);
	cos.setCanChangeOptions(true);
	cos.setCanPublishPosts(true);
	return cos;
	}
	
	public static User createAdmin(){
		
		User cos1 = new User();
		cos1.setName("Marek");
		cos1.setPassword("password");
		cos1.setEmail("email");
		cos1.setCanAccessPremium(true);
		cos1.setCanPromoteAdmins(true);
		return cos1;
		
		
		
	}
	public static  User createPremium(){ 
		
		User cos = new User();
		
		cos.setName("Wojtek");
		cos.setPassword("password");
		cos.setEmail("email");
		cos.setCanAccessPremium(true);
		return cos;
		}
public static  User createAnnon(){ 
		
		User cos = new User();
		
		cos.setName("null");
		cos.setPassword("null");
		cos.setEmail("null");
		return cos;
		}
}
