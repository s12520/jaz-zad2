package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import creators.normalUser;
import repositories.UserRepository;


@WebServlet("/logout")
public class LogoutUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		UserRepository repository =  initServlet.repository;	
		repository.addActiveUser(normalUser.createAnnon());
		session.setAttribute("conf", normalUser.createAnnon());
		response.sendRedirect("homepage.jsp");		
	}
}

