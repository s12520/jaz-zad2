package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import repositories.DummyUserRepository;
import domain.User;


@WebServlet("/showUser")
public class ShowUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	//	User application = retriveApplicationFromRequest(request);
		List<User> repository = DummyUserRepository.getMeList();
		User activeUser = DummyUserRepository.getActiveUser();
		
		
		response.setContentType("text/html");
		response.getWriter().println(
				"Imie:				 "+activeUser.getName()+"</br>"+
				"Adres Email:		 "+activeUser.getEmail()+"</br>"+
				"Haslo:              "+activeUser.getPassword()+"</br>"+
				"Konto premium:      "+activeUser.isCanAccessPremium()+"</br>"+
				"Dostep do podstron: "+activeUser.isCanAccessSubpages()+"</br>"+
				"jest premium:		 "+activeUser.isCanChangeOptions()+"</br>"+
				"jest premium: 		 "+activeUser.isCanEditPosts()+"</br>"+
				"Moze sie logowac: 	 "+activeUser.isCanLogIn()+"</br>"+
				"Jest Adminem: 		 "+activeUser.isCanPromoteAdmins()+"</br>"+
				"jest premium: 		 "+activeUser.isCanPublishPosts()+"</br>"+
				"jest premium: 		 "+activeUser.isCanRegister()+"</br>"+
				"widzi profil: 		 "+activeUser.isCanSeeProfile()
				);
		if(activeUser.isCanAccessPremium()==true){
			response.getWriter().println("</br></br>masz dostep do premium   ");	
			response.getWriter().println("<a href='premium.jsp'>premium page</a></br>");			
			}
		if(activeUser.isCanPromoteAdmins()==true){
			response.getWriter().println("jestes adminem    ");	
			response.getWriter().println("<a href='admin.jsp'>admin page</a></br>");			
			}
		
			response.getWriter().println("</br><a href='logout'>logout</a>");
		
				for(int i = 0;i<repository.size();i++){
				}
	} 
	
}