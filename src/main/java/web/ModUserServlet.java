package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import repositories.DummyUserRepository;
import domain.User;


@WebServlet("/modUser")
public class ModUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User application = retriveApplicationFromRequest(request);
		List<User> repository = DummyUserRepository.getMeList();
		response.getWriter().println("</br><a href='showUser'>powrot</a>");
		for(int i = 0;i<repository.size();i++){
			if(application.getName().equals(repository.get(i).getName())){
				User moddedUser = repository.get(i);	
				moddedUser.setCanAccessPremium(true);
				repository.remove(i);
				repository.add(moddedUser);
				response.sendRedirect("/show");
				break;
			}
		}		
	}
	
	private User retriveApplicationFromRequest (HttpServletRequest request){
		User result = new User();
		result.setName(request.getParameter("username"));
		return result;
	}
}