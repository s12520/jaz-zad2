package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import creators.normalUser;
import repositories.UserRepository;
import repositories.DummyUserRepository;



@WebServlet("/init")
public class initServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static UserRepository repository = new DummyUserRepository();
	
	protected  void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		if(repository.count()==0){
			repository.add(normalUser.createNormal());
			repository.add(normalUser.createAdmin());
			repository.add(normalUser.createPremium());
			repository.add(normalUser.createAnnon());
		}
		repository.addActiveUser(normalUser.createAnnon());
		session.setAttribute("conf", normalUser.createAnnon());
	
		response.sendRedirect("homepage.jsp");
		
	}
  	

	
}

