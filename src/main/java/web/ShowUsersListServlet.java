package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import repositories.DummyUserRepository;
import domain.User;


@WebServlet("/show")
public class ShowUsersListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		List<User> repository = DummyUserRepository.getMeList();
		response.setContentType("text/html");
	for(int i = 0;i<repository.size();i++){

		response.getWriter().println(repository.get(i).getName()
				+" is premium: "+repository.get(i).isCanAccessPremium()+"</br>");
}
	response.getWriter().println("</br>Pokaz uzytkownika<form action='showUser' method='get'>"
			+ "<label>Username:<input type='text' id='username' name='username'/>"
			+ "</label></br><input type = 'submit' value='wyslij'/></form>");
	
	response.getWriter().println("</br>Nadaj Premium: <form action='modUser' method='get'>"
			+ "<label>Username:<input type='text' id='username' name='username'/>"
			+ "</label></br><input type = 'submit' value='wyslij'/></form>");
	
	response.getWriter().println("</br><a href='showUser'>powrot</a>");
	}
}