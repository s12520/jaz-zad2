package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class DummyUserRepository  implements UserRepository {
	
	
	private static User Auser = new User(); ///
	
	private static List<User> db
							= new ArrayList<User>();
	
	@Override
	public User getApplicationByEmailAddress (String email){
		for (User application: db){
			if (application.getEmail().equalsIgnoreCase(email))
				return application;
		}
		return null;
	}

	@Override
	public void add(User application){
		db.add(application);
	}
	
	@Override
	public int count(){
		return db.size();
	}
	@Override
	public List<User> Lista(){
		return db;
	}
	@Override
	public User remove(int cos){
		 User removeIndex = db.remove(cos);
		return removeIndex;
	}
	@Override
	public void   addActiveUser (User person){
		
		Auser = person;

	}
	
	public static User getActiveUser(){
		
		return Auser;
	}
	
	public static List<User> getMeList() {
		return db;
	}	
}
