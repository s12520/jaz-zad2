package repositories;

import java.util.List;

import domain.User;

public interface UserRepository {
	
	User getApplicationByEmailAddress (String email);
		void add(User application);
		int count();
		List<User> Lista();
		User remove(int cos);
		void addActiveUser(User person);
		
	}


