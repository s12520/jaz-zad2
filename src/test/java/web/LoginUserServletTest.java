package web;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;
import web.LoginUserServlet;

public class LoginUserServletTest extends Mockito {

	@Test
	public void test () {
		fail("Not yet implemented");
	}
	@Test
	public void servlet_should_not_show_table_if_the_inquiry_is_empty() throws IOException, Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		LoginUserServlet servlet = new LoginUserServlet();
		
		when(request.getParameter("username")).thenReturn("Marek");
		when(request.getParameter("password")).thenReturn("password");
		
		servlet.doGet(request, response);
		
		
		verify(response).sendRedirect("/showUser");
	

	}


	
}
