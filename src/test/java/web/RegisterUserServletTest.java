package web;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import web.RegisterUserServlet;

public class RegisterUserServletTest extends Mockito {

	@Test
	public void test() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		RegisterUserServlet servlet = new RegisterUserServlet ();
		when(request.getParameter("username")).thenReturn("Zenek");
		when(request.getParameter("password")).thenReturn("Zenek");
		
servlet.doGet(request, response);
		
		
		verify(response).sendRedirect("/showUser");
		
	}

	

}
